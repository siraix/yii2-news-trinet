<?php

namespace app\models\news;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string $title
 * @property string $short_description
 * @property string $description
 * @property string $created_at
 */
class NewsRecord extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['title', 'description'], 'required'],
            [['short_description', 'description'], 'string'],
            [['created_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at',]
                ],
                'value' => new Expression('NOW()')
            ]];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'short_description' => 'Короткое описание',
            'description' => 'Описание',
            'created_at' => 'Создано',
        ];
    }

    /**
     * @return NewsQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return new NewsQuery(get_called_class());
    }
}
