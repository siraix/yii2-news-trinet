<?php

namespace app\models\news;

/**
 * This is the ActiveQuery class for [[NewsRecord]].
 *
 * @see NewsRecord
 */
class NewsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return NewsRecord[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return NewsRecord|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
